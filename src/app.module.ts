import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/yaml.config';
import { MongooseModule } from '@nestjs/mongoose';
import { ActivitylogModule } from './activitylog/activitylog.module';
@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('mongodb.url'),
        dbName: 'db_log_salesforce',
      }),
      inject: [ConfigService],
    }),
    ConfigModule.forRoot({ isGlobal: true, load: [configuration] }),
    ActivitylogModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
