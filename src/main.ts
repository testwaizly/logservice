import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { envObjStart } from './config/envsub.config';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import * as Sentry from '@sentry/node';

import { ConfigService } from '@nestjs/config';
import { ExceptionFilter } from './common/rpc-exception.filter';
// import { AllExceptionsFilter } from './common/rpc-exception.filter';
async function bootstrap() {
  await envObjStart();
  const app = await NestFactory.create(AppModule);
  // await app.init();
  const configService = app.get(ConfigService);
  const user = configService.get('rabbitmq.user');
  const password = configService.get('rabbitmq.password');
  const host = configService.get('rabbitmq.host');
  const queueName = configService.get('rabbitmq.queuename');
  app.useGlobalFilters(new ExceptionFilter());
  // app.enableCors({ origin: true });
  // const logger = new Logger('App');
  // security
  Sentry.init({ dsn: configService.get('sentry.dsn') });
  const rabbit = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [`amqp://${user}:${password}@${host}`],
      noAck: false,
      persistent: true,
      queue: queueName,
      queueOptions: {
        durable: true,
        queueMode: 'lazy',
        passive: true,
      },
    },
  });
  await rabbit.listen();
  console.log(
    `🚀 User service running on port ${configService.get('http.port')}`,
  );
}
bootstrap();
