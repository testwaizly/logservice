import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  UserActivity,
  UserActivityDocument,
} from './model/useractivity.entity';
import { RpcException } from '@nestjs/microservices';
import { Model } from 'mongoose';

@Injectable()
export class ActivitylogService {
  constructor(
    @InjectModel(UserActivity.name)
    private readonly userActivitymodel: Model<UserActivityDocument>,
  ) {}
  async createActivityLog(createCatDto: any): Promise<any> {
    try {
      const createdCat = new this.userActivitymodel(createCatDto);
      return createdCat.save();
    } catch (error) {
      throw new RpcException({
        statusCode: 404,
        message: 'data not found',
      });
    }
  }
  async getActivityLog(): Promise<any[]> {
    const oneMonthLater = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000); // menghitung tanggal 30 hari ke depan
    return this.userActivitymodel
      .find({ createdAt: { $lte: oneMonthLater } })
      .sort({ createdAt: 'desc' })
      .exec();
  }

  async getSingleUserActivity(id: string): Promise<any> {
    return this.userActivitymodel.findOne({ _id: id }).exec();
  }
}
