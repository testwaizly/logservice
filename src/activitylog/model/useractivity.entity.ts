import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { now, Document } from 'mongoose';
export type UserActivityDocument = UserActivity & Document;

@Schema()
export class UserActivity {
  @Prop({ type: Array, default: null })
  user: any;
  @Prop()
  ipaddress: string;
  @Prop()
  user_agent: string;
  @Prop()
  action: string;
  @Prop()
  messages: string;
  @Prop({ type: Array, default: null })
  before: any;
  @Prop({ type: Array, default: null })
  after: any;
  @Prop()
  createdAt: Date;
}

export const UserActivitySchema = SchemaFactory.createForClass(UserActivity);
