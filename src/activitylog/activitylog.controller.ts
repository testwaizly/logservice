import { Controller } from '@nestjs/common';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { ActivitylogService } from './activitylog.service';
@Controller('activitylog')
export class ActivitylogController {
  constructor(private userActivityService: ActivitylogService) {}
  @MessagePattern('log-get-useractivity')
  async getActivityLog(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const roles = await this.userActivityService.getActivityLog();
    return roles;
  }
  @MessagePattern('log-create-useractivity')
  async createDelimeterType(@Payload() body: any, @Ctx() context: RmqContext) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();
    channel.ack(originalMsg);
    const roles = await this.userActivityService.createActivityLog(body);
    return roles;
  }
}
