import { Module } from '@nestjs/common';
import { ActivitylogService } from './activitylog.service';
import { ActivitylogController } from './activitylog.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserActivity, UserActivitySchema } from './model/useractivity.entity';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: UserActivity.name,
        useFactory: (configService: ConfigService) => {
          const connection = configService.get<string>('mongodb.url');
          const schema = UserActivitySchema;
          return connection ? schema : null;
        },
        inject: [ConfigService],
      },
    ]),
  ],
  providers: [ActivitylogService],
  controllers: [ActivitylogController],
})
export class ActivitylogModule {}
